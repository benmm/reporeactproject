import '../App.css';
import React,{useContext} from 'react'


import { AppContext } from "../App";
import Item from './Item';

function Items() {    
  const { list } = useContext(AppContext);

  return (
    
    <ul>{list.map((item=>
      <Item item={item}/>
      )
      )}</ul>
  )
}

export default Items